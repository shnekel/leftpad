package leftpad

import (
	"strings"
)

// Adding left padding
// This func is just for training in creating go module
func LeftPad(length int, opts ...string) string {
	var sb strings.Builder
	for i := 0; i < length-len(opts[0]); i++ {
		if len(opts) > 1 {
			sb.WriteString(opts[1])
		} else {
			sb.WriteString("0")
		}
	}
	sb.WriteString(opts[0])
	return sb.String()
}
