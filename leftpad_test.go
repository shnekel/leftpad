package leftpad

import "testing"

func TestLeftPad(t *testing.T) {
	res := LeftPad(3, "1")

	if res != "001" {
		t.Error("Test failed", res)
	}

}
func TestLeftPadNoPad(t *testing.T) {
	res := LeftPad(5, "hello")

	if res != "hello" {
		t.Error("Test failed", res)
	}
}

func TestLeftPadWithChar(t *testing.T) {
	res := LeftPad(3, "1", "x")

	if res != "xx1" {
		t.Error("Test failed", res)
	}
}
